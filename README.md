Teodor Alling
BINP29, February 2023
# Background
This work was conducted as part of the course BINP29 in february 2023. The goal was to go through the typical procedure of a long read, *de-novo* genome assembly. Starting from sequenced reads (HiFi Pacbio), we first check their quality before assembling using more than one assembly software. Multiple assemblies of the same reads can then be compared using basic statistics as well as on their assembly completeness.
Another goal was to follow appropriate directory structure and work documentation, including version control with git and github.
# Programs used
- fastq-dump 3.0.3 (from sra-tools) (used to download read data from SRA database)
- FastQC v0.11.9 (used for quality control of sequenced reads)
- trimmomatic 0.39 (used to crop reads)
- Hifiasm 0.18.8-r525 (used for *de novo* assembly) (Reference: Cheng, H., Concepcion, G.T., Feng, X., Zhang, H., Li H. (2021) Haplotype-resolved de novo assembly using phased assembly graphs with hifiasm. Nat Methods, 18:170-175. https://doi.org/10.1038/s41592-020-01056-5)
- gfa-to-fasta.py by https://gist.github.com/fedarko (no version number - accessed 2023-02-25) (used to convert .gfa assembly file to .fasta)
- Improved Phased Assembly version=1.8.0 (used for *de novo* assembly) (Reference: no article but see https://github.com/PacificBiosciences/pbipa, creators are PacificBiosciences)
- QUAST v5.2.0 (used to get basic assembly statistics in order to compare assemblies)
- BUSCO 5.4.4 (used to assess the completeness of assembled genomes)
All softwares except for the gfa-to-fasta.py were installed and managed using conda.
# Data used
The starting dataset consists of HiFi Pacbio reads from the yeast _Saccharomyces cerevisiae_ and was downloaded from NCBI Sequence Read Archive (SRA) at https://trace.ncbi.nlm.nih.gov/Traces?run=SRR13577846. The following commands were used to download and control the .fastq file:
```bash
# Download .fastq format of the given accession ID in SRA:
fastq-dump SRR13577846
# Change to read-only:
chmod -w SRR13577846.fastq 

# Check number of reads and that it is complete:
sed -n '1~4p' SRR13577846.fastq | wc -l # Count putative number of entries
echo $(cat SRR13577846.fastq | wc -l)/4 | bc # Count lines in the fastq file divided by 4
# They are the same number; 117525 reads. This also matches the number of spots reported in the SRA web entry metadata.

# Check number of bases:
sed -n '2~4p' SRR13577846.fastq | tr -d '\n' | wc -c
# 1 103 860 593 characters. Matches number of bases reported in the SRA web entry metadata.
```
Average read length is 1103860593/117525 = 9393 bp/read.
# Walkthrough
## 1. Prepare data
### 1.1. Quality control
```bash
fastqc -o 1_read_quality/fastqc_results/ -f fastq Data/SRR13577846.fastq
```
Three measures give warning or error:
Per base sequence content gives error. The nt G is extremely overrepresented toward the end of the sequences. This might be because of some adapter.
Per base CG content also gives an error. Mean GC content of around 38% per read is extremely overrepresented. This indeictes some specific motif, and could be adapter. It is possible that it is the high G content towards the ends of the reads (see above) contributes to this specific 38% GC content. I fear that this might cause problems during assembly.
Sequence length distribution gives a warning, but I do not think it is worrisome since there is a sharp peak and while there is some variation in sequence length it is not that massive.
The rest of the fastqc measures look OK, both to me and to the program.
Note: I would also preferrably check the reads for contamination using FastQ screen with various reference genomes. However, time is of the essence and I will skip this step for now.
### 1.2. Trimming
As we suspect that the reads contain adapters I will trim the ends. The overrepresentation appears only after about 12900 position (and therefore only in maximum about 500 or so reads), so we use that as crop:
```bash
trimmomatic SE -phred64 Data/SRR13577846.fastq Data/crop12900.fastq CROP:12900
```
Note I only cropped, and did not trim on quality as quality is already acceptable throughout the reads.
Check quality again:
```bash
fastqc -o 1_read_quality/fastqc_results_after_trimming/ -f fastq Data/crop12900.fastq
```
Now there is only warning for Per base sequence content. It looks alright to me. The Per sequence GC content still gives error so it was not the overrepresentation in the tails that caused this. I will continue with assembly nonetheless as some elevated GC content is accepted.
## 2. Assembly
### 2.1. Hifiasm
With Hifiasm, perform *de novo* assembly (timed):
```bash
time hifiasm -t 4 Data/crop12900.fastq
cp 2_assemblies/hifiasm/hifiasm.asm.p_ctg.gfa 2_assemblies/hifiasm_primary.gfa
```
Time results:
real	37m9.864s.
user	145m41.417s.
sys	0m40.757s.

Convert to .fasta using https://gist.github.com/fedarko/9fe32014f1e55d80511be0d22dc36830#file-gfa-to-fasta-py:
```bash
python gfa-to-fasta.py 2_assemblies/hifiasm_primary.gfa 2_assemblies/hifiasm_primary.fasta
```
### 2.2. Improved Phased Assembler
With Improved Phased Assembler (IPA), perform *de novo* assembly (timed):
```bash
time ipa local --nthreads 4 --njobs 1 -i Data/crop12900.fastq --run-dir 2_assemblies/ipa
cp 2_assemblies/ipa/19-final/final.p_ctg.fasta 2_assemblies/ipa_primary.fasta
```
Time results:
real	66m26.283s.
user	231m7.420s.
sys	2m11.479s.
## 3. Evaluate assembly quality
Evaluate both assemblies at once using quast:
```bash
time quast -o 3_assembly_quality/quast -t 4 --fungus --est-ref-size 12156677 2_assemblies/hifiasm_primary.fasta 2_assemblies/ipa_primary.fasta
# -o for output directory, -t for 4 threads, --fungus to specify the genome is fungal, --est-ref-size to specify estimated genoem size which is necessary to calculate NGx statistics (reference size obtained from https://en.wikipedia.org/wiki/Saccharomyces_cerevisiae)
```
Time results:
real	0m3.099s.
user	0m7.457s.
sys	0m27.736s.
### 3.1. Some results
N50 is the size of the smallest contig in the set of largest contigs that make up 50% of the **assembly size**.
N50 hifiasm: 809047.
N50 ipa: 807826.
NG50 is the size of the smallest contig in the set of largest contigs that make up 50% of the **estimated genome size**.
NG50 hifiasm 809047.
NG50 ipa: 807826.
In this case, these are the same values. Likely because the data we used is very similar to the data used to calculate the genome size for this organism.
## 4. Evaluate assembly completeness
### 4.1. Evaluate Hifiasm assembly completeness
```bash
Evaluate Hifiasm assebly using BUSCO:
time busco -i 2_assemblies/hifiasm_primary.fasta -l saccharomycetes_odb10 -o 4_assembly_completeness/hifiasm -m genome -c 4
# -i for input file, -l for BUSCO lineage data to be used, -o for output directory, -m for mode, -c for number of cores.
```
Total running time: 255 seconds (as reported by the program's terminal print).
### 4.2. Evaluate IPA assembly completeness
```bash
# Remove '/' from input fasta file, or busco will throw error:
cat 2_assemblies/ipa_primary.fasta | tr '/' '_' > 2_assemblies/ipa_primary_edited.fasta
# Evaluate IPA assebly using BUSCO:
time busco -i 2_assemblies/ipa_primary_edited.fasta -l saccharomycetes_odb10 -o 4_assembly_completeness/ipa -m genome -c 4
# -i for input file, -l for BUSCO lineage data to be used, -o for output directory, -m for mode, -c for number of cores.
```
Total running time: 248 seconds (as reported by the program's terminal print).